import { Component } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
	fullname:string = '';
	errorfullname = false;
	emailaddress:string = '';
	erroremailaddress = false;
	erroremailaddressvalide = false;
	mobilenum = '';
	errormobilenum = false;
	demo_store = [];
	constructor(private router: Router) {
		
		var demo_store = JSON.parse(localStorage.getItem('demo_store1'));
		this.demo_store = (demo_store);
			console.log(demo_store);
	}
	
	submit_button() {
		var iserror = 'no';
		this.errorfullname = false;
		this.erroremailaddress = false;
		this.erroremailaddressvalide = false;
		this.errormobilenum = false;
		console.log(this.fullname);
		console.log(this.emailaddress);
		console.log(this.mobilenum);
		if(!this.fullname){
			iserror = 'yes';
			this.errorfullname = true;
		}
		if(!this.emailaddress){
			iserror = 'yes';
			this.erroremailaddress = true;
		}else{
			var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
			if((!EMAIL_REGEXP.test(this.emailaddress))){
				this.erroremailaddressvalide = true;
				iserror = 'yes';
			}
		}
		if(!this.mobilenum){
			iserror = 'yes';
			this.errormobilenum = true;
		}
		
		if(iserror == 'no'){
			console.log('final submit')
			const dataarry = [];
			 dataarry['fullname'] = this.fullname;
			 dataarry['emailaddress'] = this.emailaddress;
			 dataarry['mobilenum'] = this.mobilenum;
			const comman_data = {
				fullname: this.fullname,
				emailaddress: this.emailaddress,
				mobilenum: this.mobilenum,
			};
			var mydata = [];
			mydata = this.demo_store;
			mydata.push(comman_data);
			console.log(dataarry);
			console.log(mydata);
			localStorage.setItem('demo_store1' , JSON.stringify(mydata));
			
			var demo_store = JSON.parse(localStorage.getItem('demo_store1'));
			console.log(demo_store);
			this.router.navigate(['listing']);
		}
	}
}
