import { Component } from '@angular/core';

@Component({
  selector: 'app-listing',
  templateUrl: 'listing.page.html',
  styleUrls: ['listing.page.scss'],
})
export class ListingPage {
	
	demo_store = [];
	constructor() {
		
		var demo_store = JSON.parse(localStorage.getItem('demo_store1'));
		this.demo_store = (demo_store);
		console.log(this.demo_store);
	}
	
}
